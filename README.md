#Tetris Game

Java implementation of the famous game Tetris (required JAVA version >= 9)

Run the Game `./gradlew run` or simply `./gradlew`

Run the test `./gradlew test`

Create a jar with all the dependency `./gradlew createJar`

##Software Requirements Specification

I vari pezzi del gioco di Tetris si chiamano tetramini, ciascuno composto da quattro blocchi, infatti il termine Tetris 
deriva, come detto sopra, da tetramino. I tetramini cadono giù uno alla volta e il compito del giocatore è ruotarli e/o 
muoverli in modo che creino una riga orizzontale di blocchi senza interruzioni. Quando la riga è stata creata, 
i mattoni spariscono e i pezzi sovrastanti (se presenti) cadono a formare nuove linee.
I pezzi di Tetris: I, J, L, O, S, T, Z.

Le sette possibili combinazioni in Tetris sono chiamate come le corrispondenti lettere dell'alfabeto che più si 
avvicinano alla forma del pezzo: I, T, O, L, J, S, e Z. Le forme dei tetramini sono il risultato di tutte 
le possibili combinazioni che si possono ottenere disponendo 4 quadrati, ciascuno dei quali ha almeno un lato 
in comune con almeno uno degli altri tre quadrati.

La modalità di gioco e' la seguente:
Lo schermo inizialmente è vuoto. La velocità di caduta dei tetramini aumenta ogni 10 nuovi pezzi e la partita termina 
quando i mattoncini raggiungono la sommità dello schermo. 

##Software Design Description

###Tetromino Package

AbstractTetromino class

Abstract class che definisce la struttura di un Tetromino e tutti i tipi ammisibili.
I discendenti devono implementare i metodi `abstract List<Point> getPoints(); abstract Color getColor();`
Implementa l'interfaccia Rotate.

Rotate<T> interface
Interfccia che definisce i metodi per rotazione di un classe T. I metodi `rotateLeft(); T rotateRight();` 
ritornano il tipo T.

Tetromino class

Classe concreta che estende AbstractTetromino. 
Non ha un costruttore pubblico e quindi necessita di TetrominoFactory per instanziare
la classe. Tutti i riferimenti agli attributi che ritorna la classe sono copie(defensive strategy).
Implementa i metodi dell'interfaccia Rotate per la rotazione di un Tetromino.

Tetromino_O class

Estende la classe Tetromino per il tipo "O" poiche' e' l'unico tetronimo con un comportamento differente 
durante la rotazione.

AbstractFactory<T,N> interface

Interfaccia per la definizione di una abstract factory. Il metodo create ritorna un tipo di classe T dato un input 
di tipo N.

TetrominoFactory class

Implementa l'interfaccia AbstractFactory ed e' responsabile per la creazione di oggetti Tetromini.

###Restanti Classi

TetrisBoardInterface interface

Definisce il compotamento che una Board in Tetris deve avere.

Definisce i metodi

```
    void addNewPiece(Tetromino piece);
    void dropDown();
    void moveLeft();
    void moveRight();
    void rotate();
    void clearRows();
```

Board class

Implementa l'interfaccia TetrisBoardInterface e implementa i metodi da essa definita.
Implementa l'interfaccia Flow.Publisher (Pattern Publisher-Subscriber). La classe Tetris sara' il subscriber.
Estende la classe JPanel ed e' la classe incaricata di disegnare gli elementi del gioco.

Tetris class

Implementa l'interfaccia Flow.Subscriber ed aspetta eventi dall'attributo board.
E' incaricato di intecettare gli input dall'utente e inizializzare il gioco e la creazioni di nuovi Tetromini 
da aggiongere alla board. La creazione e' realizzata utilizzando il pattern Stategy.

CreationStrategy<T> interface
Definisce il metodo `T getType();`

TetrominoCreationStrategy class

Implementa CreationStrategy<Tetromino.Type> e definisce la strategia(random) per la selezione del tipo Tetromino.Type.

