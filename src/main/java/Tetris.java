import tetronimo.Tetromino;
import tetronimo.TetrominoFactory;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Flow;

public class Tetris implements Flow.Subscriber<String> {

    private Flow.Subscription subscription;
    private TetrominoFactory factory;

    private JFrame frame ;
    private Board board = new Board();
    private boolean isGameOver;

    private final int VELOCITY = 1000;

    private CreationStrategy<Tetromino.Type> creationStrategy;

    public Tetris(CreationStrategy creationStrategy, TetrominoFactory factory) {
        frame = new JFrame("Tetris");
        board.subscribe(this);
        isGameOver = false;

        this.creationStrategy = creationStrategy;
        this.factory = factory;
    }

    // Put a new, random piece into the dropping position
    public Tetromino newPiece() {
        Tetromino.Type type = creationStrategy.getType();
        return factory.create(type);
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(Long.MAX_VALUE); //TODO: Is correct to put this as unbounded?
        board.addNewPiece(newPiece());
    }

    @Override
    public void onNext(String o) {
        board.addNewPiece(newPiece());
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();

    }

    @Override
    public void onComplete() {
        isGameOver = true;
    }

    public void init() {
        final int SQUARE_WIDTH = 26;
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(board.TETRIS_COL * SQUARE_WIDTH, (board.TETRIS_ROW + 1) * SQUARE_WIDTH);
        frame.setVisible(true);

        frame.add(board);

        // Keyboard controls
        frame.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        board.rotate();
                        break;
                    case KeyEvent.VK_DOWN:
                        board.dropDown();
                        break;
                    case KeyEvent.VK_LEFT:
                        board.moveLeft();
                        break;
                    case KeyEvent.VK_RIGHT:
                        board.moveRight();
                        break;
                    case KeyEvent.VK_SPACE:
                        board.dropDown();
                        break;
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        });

        // Make the falling piece drop every second
        Thread t = new Thread(() -> runGame());
        t.start();
    }

    void runGame() {

        while (!isGameOver) {
            try {
                Thread.sleep(VELOCITY);
                if(!isGameOver) board.dropDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        closeTetrisFrame();
        showGameOverMessage();
    }

    private void closeTetrisFrame() {
        frame.setVisible(false);
        frame.dispose();
    }
    private void showGameOverMessage(){
        JOptionPane.showConfirmDialog(null,
                "Game Over!",
                "Tetris Game",
                JOptionPane.WARNING_MESSAGE);
    }

}



