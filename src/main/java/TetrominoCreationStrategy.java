import tetronimo.Tetromino;

public class TetrominoCreationStrategy implements CreationStrategy<Tetromino.Type> {

    public Tetromino.Type getType(){

        double rand = Math.random();

        Tetromino.Type values [] = Tetromino.Type.values();

        int size = values != null ? values.length : 0;

        if(size == 0) throw new IllegalStateException("No Tetronimo Type Declared"); //TODO Check if the exception is correct

        int index = (int) Math.floor(rand * size);

        return values[index];
    }
}
