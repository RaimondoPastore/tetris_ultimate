import tetronimo.Tetromino;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

public class Board extends JPanel implements TetrisBoardInterface, Flow.Publisher {

    SubmissionPublisher<String> publisher = new SubmissionPublisher<>();

    public final int TETRIS_COL = 13;
    public final int TETRIS_ROW = 24;

    private Tetromino currentPiece;

    private final Color[][] board;

    private int down = 0;
    private int traslate = 0;

    public Board() {
        board = new Color[TETRIS_ROW][TETRIS_COL];

        for (int i = 0; i < TETRIS_ROW - 1; i++) {
            for (int j = 0; j < TETRIS_COL; j++) {
                board[i][j] = Color.BLACK;
            }
        }

        for (int i = 0; i < TETRIS_COL; i++) {
            board[TETRIS_ROW - 1][i] = Color.GRAY;
        }
    }


    @Override
    public void addNewPiece(Tetromino piece) {
        currentPiece = piece;
        down = 1;
        traslate = 6;

        if (collidesAt(currentPiece, 0, 0)) {
            publisher.close();
        }

    }

    @Override
    public void dropDown() {

        if (!collidesAt(currentPiece, 0, 1)) {
            down++;
        } else {
            fixToBoard();
        }
        repaint();
    }

    // Collision test for the dropping piece
    private boolean collidesAt(Tetromino t, int moveX, int moveY) {

        for (Point p : t.getPoints()) {
            int y = p.y + down + moveY;
            int x = p.x + traslate + moveX;

            if (y < 0 || y == TETRIS_ROW || x == TETRIS_COL || x < 0 || board[y][x] != Color.BLACK) {
                return true;
            }
        }
        return false;
    }

    // Move the piece left or right
    public void moveLeft() {

        if (!collidesAt(currentPiece, -1, 0)) {
            traslate--;
            repaint();
        }

    }

    public void moveRight() {

        if (!collidesAt(currentPiece, 1, 0)) {
            traslate++;
            repaint();
        }
    }

    public void rotate() {
        if (!collidesAt(currentPiece.clone().rotateRight(), 0, 0)) {
            currentPiece.rotateRight();
            repaint();
        }
    }


    // Make the dropping piece part of the well, so it is available for
    // collision detection.
    public void fixToBoard() {

        for (Point p : currentPiece.getPoints()) {
            int a = down + p.y;
            int b = traslate + p.x;

            if (a >= 0 && b >= 0 && a < TETRIS_ROW && b < TETRIS_COL) {
                board[a][b] = currentPiece.getColor();
            }
        }
        clearRows();
        if (!publisher.isClosed()) publisher.submit("Fix To the Board");
    }


    public void clearRows() {
        int fullRows = 0;
        for (int j = TETRIS_ROW - 2; j > 0; j--) {
            if (isFullRow(board[j])) {
                deleteRow(j);
                j += 1;
                fullRows += 1;
            }
        }
    }

    public void deleteRow(int row) {
        for (int j = row - 1; j > 0; j--) {
            for (int i = 1; i < TETRIS_COL - 1; i++) {
                board[j + 1][i] = board[j][i];
            }
        }
    }


    boolean isFullRow(Color[] row) {
        for (int i = 0; i < TETRIS_COL; i++) {
            if (row[i].equals(Color.BLACK)) {
                return false;
            }
        }
        return true;
    }

    private void drawPiece(Graphics g) {

        g.setColor(currentPiece.getColor());

        for (Point p : currentPiece.getPoints()) {
            g.fillRect((p.x + traslate) * (TETRIS_ROW + 2),
                    (p.y + down) * (TETRIS_ROW + 2),
                    (TETRIS_ROW + 1), (TETRIS_ROW + 1));
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        //Paint the board
        g.fillRect(0, 0, TETRIS_ROW, TETRIS_COL);
        for (int i = 0; i < TETRIS_ROW; i++) {
            for (int j = 0; j < TETRIS_COL; j++) {
                g.setColor(board[i][j]);
                g.fillRect((TETRIS_ROW + 2) * j, (TETRIS_ROW + 2) * i, (TETRIS_ROW + 1), (TETRIS_ROW + 1));
            }
        }

        // Draw the current piece in the board
        drawPiece(g);
    }

    @Override
    public void subscribe(Flow.Subscriber subscriber) {
        publisher.subscribe(subscriber);
    }
}

