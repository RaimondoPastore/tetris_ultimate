import tetronimo.Tetromino;

public interface CreationStrategy<T> {
    T getType();
}
