package tetronimo;

import java.awt.*;

public class TetrominoFactory implements AbstractFactory<Tetromino, Tetromino.Type> {

    @Override
    public Tetromino create(Tetromino.Type type) {

        switch (type) {
            case I: {
                return new Tetromino(Color.blue,
                        new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, -1)
                );
            }
            case O: {
                return new Tetromino_O(Color.red,
                        new Point(-1, 1), new Point(0, 0), new Point(0, 1), new Point(-1, 0)
                );
            }
            case T: {
                return new Tetromino(Color.green,
                        new Point(0, -1), new Point(0, 0), new Point(1, 0), new Point(-1, 0)
                );
            }
            case S: {
                return new Tetromino(Color.yellow,
                        new Point(-1, 1), new Point(0, 0), new Point(0, 1), new Point(1, 0)
                );
            }
            case Z: {
                return new Tetromino(Color.orange,
                        new Point(1, 1), new Point(0, 0), new Point(0, 1), new Point(-1, 0)
                );
            }
            case L: {
                return new Tetromino(Color.pink,
                        new Point(-1, 1), new Point(0, 0), new Point(1, 0), new Point(-1, 0));
            }
            case J: {
                return new Tetromino(Color.magenta,
                        new Point(1, 1), new Point(0, 0), new Point(1, 0), new Point(-1, 0)
                );
            }
            default: {
                throw new IllegalArgumentException("Wrong Tetromino Type");
            }
        }

    }

}
