package tetronimo;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Tetromino extends AbstractTetromino {

    Tetromino(Color color, Point p1, Point p2, Point p3, Point p4) {
        this.points = List.of(p1, p2, p3, p4);
        this.color = color != null ? new Color(color.getRGB()) : new Color(0);
    }

    @Override
    public Tetromino rotateLeft() {
        points.stream().forEach(this::rotatePointToRight);
        return this;
    }

    @Override
    public Tetromino rotateRight() {
        points.stream().forEach(this::rotatePointToLeft);
        return this;
    }

    @Override
    public List<Point> getPoints() {
        return clonePoints();
    }

    @Override
    public Color getColor() {
        return new Color(color.getRGB());
    }

    public Tetromino clone() { //TODO: should I create the clone interface or in Abstract class or leave here
        List<Point> clonedPoints = clonePoints();
        return new Tetromino(color, clonedPoints.get(0), clonedPoints.get(1), clonedPoints.get(2), clonedPoints.get(3));
    }

    private void rotatePointToRight(Point p) {

        int y = (int) p.getY();
        int x = (int) p.getX();

        p.move(y, -x);
    }

    private void rotatePointToLeft(Point p) {

        int y = (int) p.getY();
        int x = (int) p.getX();

        p.move(-y, x);
    }

    private List<Point> clonePoints() {

        if (points != null) {
            List<Point> copyPoints = points.stream().map(Point::new).collect(Collectors.toList());
            return copyPoints;
        } else {
            return new ArrayList<Point>();
        }
    }

}
