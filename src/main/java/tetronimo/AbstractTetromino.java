package tetronimo;

import java.awt.*;
import java.util.List;

abstract class AbstractTetromino implements Rotate {

    public enum Type {I, O, T, S, Z, L, J}

    protected Color color;
    protected List<Point> points;

    abstract List<Point> getPoints();
    abstract Color getColor();

}
