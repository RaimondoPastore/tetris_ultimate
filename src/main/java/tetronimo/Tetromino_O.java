package tetronimo;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Tetromino_O extends Tetromino {

    public Tetromino_O(Color color, Point p1, Point p2, Point p3, Point p4) {
        super(color, p1, p2, p3, p4);
    }

    @Override
    public Tetromino rotateLeft() {
        return this;
    }

    @Override
    public Tetromino rotateRight() {
        return this;
    }




}
