package tetronimo;

public interface AbstractFactory<T,N> {

    T create(N type);
}
