package tetronimo;

public interface Rotate<T> { // TODO: use wild card for Tetronimo_O

    T rotateLeft();
    T rotateRight();

}
