import tetronimo.Tetromino;
import java.util.concurrent.Flow;

public interface TetrisBoardInterface {

    void addNewPiece(Tetromino piece);
    void dropDown();
    void moveLeft();
    void moveRight();
    void rotate();
    void clearRows(); //TODO: return an int?
}
