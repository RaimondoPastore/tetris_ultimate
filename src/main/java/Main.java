import tetronimo.TetrominoFactory;

public class Main {

    public static void main(String[] args) {
        TetrominoCreationStrategy creationStrategy = new TetrominoCreationStrategy();
        TetrominoFactory factory = new TetrominoFactory();

        Tetris tetrisGame = new Tetris(creationStrategy, factory);
        tetrisGame.init();
    }
}