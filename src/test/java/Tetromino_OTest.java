import org.junit.Test;
import tetronimo.Tetromino;
import tetronimo.TetrominoFactory;

import java.awt.*;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class Tetromino_OTest {

    @Test
    public void testRotateRigthTetrominoI() {
        Tetromino.Type type = Tetromino.Type.O;
        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        Point[] initialStatePoints = {new Point(-2, 1), new Point(0, 0), new Point(0, 1), new Point(-1, 0)};

        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(initialStatePoints));

        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(initialStatePoints));

        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(initialStatePoints));

        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(initialStatePoints));
    }

    @Test
    public void testCloneTetrominoO() {

        Tetromino.Type type = Tetromino.Type.I;
        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        Tetromino clone = tetromino.clone();

        assertThat(clone.getPoints()).isEqualTo(tetromino.getPoints());
        assertThat(clone.getColor()).isEqualTo(tetromino.getColor());

    }

}
