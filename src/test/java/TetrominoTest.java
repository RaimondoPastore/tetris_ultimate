import org.junit.Test;
import tetronimo.Tetromino;
import tetronimo.TetrominoFactory;
import java.awt.*;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;

public class TetrominoTest {

    @Test
    public void testRotateRigthTetrominoI() {
        Tetromino.Type type = Tetromino.Type.I;
        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        Point[] firstRotatePoints = {new Point(0, 0), new Point(-1, 0), new Point(-2, 0), new Point(1, 0)};
        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(firstRotatePoints));

        Point[] secondRotatePoints = {new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, 1)};
        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(secondRotatePoints));

        Point[] thirdRotatePoints = {new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(-1, 0)};
        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(thirdRotatePoints));

        Point[] initialStatePoints = {new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, -1)};
        tetromino.rotateRight();
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(initialStatePoints));
    }

    @Test
    public void testCloneTetrominoI() {

        Tetromino.Type type = Tetromino.Type.I;
        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        Tetromino clone = tetromino.clone();

        assertThat(clone.getPoints()).isEqualTo(tetromino.getPoints());
        assertThat(clone.getColor()).isEqualTo(tetromino.getColor());

    }

}
