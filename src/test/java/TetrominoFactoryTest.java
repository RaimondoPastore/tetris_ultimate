import org.junit.Test;
import tetronimo.Tetromino;
import tetronimo.TetrominoFactory;
import tetronimo.Tetromino_O;

import java.awt.*;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;


public class TetrominoFactoryTest {

    @Test
    public void testCreationOfTypeI() {
        Tetromino.Type type = Tetromino.Type.I;
        Point[] points = {new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, -1)};

        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        assertThat(tetromino.getColor()).isEqualTo(Color.blue);
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(points));
    }

    @Test
    public void testCreationOfTypeO() {
        Tetromino.Type type = Tetromino.Type.O;
        Point[] points = {new Point(-1, 1), new Point(0, 0), new Point(0, 1), new Point(-1, 0)};

        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        assertThat(tetromino).isInstanceOf(Tetromino_O.class);
        assertThat(tetromino.getColor()).isEqualTo(Color.red);
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(points));
    }

    @Test
    public void testCreationOfTypeT() {
        Tetromino.Type type = Tetromino.Type.T;
        Point[] points = {new Point(0, -1), new Point(0, 0), new Point(1, 0), new Point(-1, 0)};

        TetrominoFactory factory = new TetrominoFactory();
        Tetromino tetromino = factory.create(type);

        assertThat(tetromino.getColor()).isEqualTo(Color.green);
        assertThat(tetromino.getPoints()).isEqualTo(Arrays.asList(points));
    }
}
